from .people import *
from .courses import *
from .academic import *
from .students import *
