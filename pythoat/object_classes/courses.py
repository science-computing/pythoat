from .baseclass import ApiObject, ApiAttr
from .people import Instructor


class Section(ApiObject):
    section_code = ApiAttr(typing=int)
    component_code = ApiAttr(typing=str)
    class_id = ApiAttr(typing=int)
    section_id = ApiAttr(typing=int)

    @property
    def is_online(self):
        return None if self.section_code is None else\
            (
                self.section_code >= 80 and
                self.section_code <= 89
            )

    @property
    def has_crosslisted(self):
        return None if self.section_id is None else\
            (self.section_id > 0)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.course = None
        self.meets = []
        self.regiestered_students = []
        self.crosslisted = []
        self.got_crosslisted = False

        self.instructors = []
        if 'instructors' in kwargs:
            for instructor in kwargs['instructors']:
                inst = Instructor(self.session, **instructor)
                self.instructors.append(inst)

    def getCrossListed(self):
        pass

    def getCourse(self):
        pass

    def __str__(self):
        return '({}) {:03d} {} {}'.format(
            self.course if self.course else '?course?',
            self.section_code,
            self.component_code,
            self.section_id,
        )


class Course(ApiObject):
    term = ApiAttr(typing=int)
    subject_code = ApiAttr(typing=str)
    catalog = ApiAttr(typing=str)
    course_short_title = ApiAttr(typing=str)
    course_tile = ApiAttr(typing=str)
    course_id = ApiAttr(typing=int)
    class_id = ApiAttr(typing=int)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sections = []
        for section in kwargs.get('sections', []):
            sec = Section(self.session, **section)
            sec.course = self
            self.sections.append(sec)

    def getSections(self):
        pass

    def __str__(self):
        return '{} {} {}'.format(
            self.subject_code,
            self.catalog,
            self.term if self.term else '?term?'
        )


