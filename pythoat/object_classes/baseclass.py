import copy

class ApiAttr:
    def __init__(
        self,
        typing=None,
        required=False,
        default=None,
    ):

        self.typing = typing
        self.required = required
        self.default = default
    def parse(self, val, session=None):
        if val is None:
            if self.required:
                raise Exception(':(')
            return self.default
        if self.typing is not None:
            if isinstance(self.typing, ApiObjectBase):
                return self.typing(session, **val)
            else:
                return self.typing(val)


class ApiListAttr:
    def __init__(self, typing, getter=None):
        self.typing=typing
        self.getter = getter

    def parse(self, val, session=None):
        if val is None:
            return []
        if isinstance(val, dict):
            val = val.values()
        print(type(self.typing))
        if isinstance(self.typing, ApiObjectBase):
            return [self.typing(session, **x) for x in val]
        else:
            return [self.typing(x) for x in val]


# Inspired by django's form metaclass
class ApiObjectBase(type):
    def __new__(cls, name, sup, attrs):
        api_attrs = {}
        # Go through all class attrs --
        for key, value in list(attrs.items()):
            # Check if they're ApiAttrs, and if so
            if value.__class__ in (ApiAttr, ApiListAttr):
            # if isinstance(value, ApiAttr):
                # Remove them and put them in this subattr
                api_attrs[key] = attrs.pop(key)
        attrs['api_attrs'] = api_attrs

        # Generate the new Object Class
        new_class = super().__new__(cls, name, sup, attrs)

        # Climb through the parent classes (if this inherits)
        for base in reversed(new_class.__mro__):
            if hasattr(base, 'api_attrs'):
                # Append the ApiAttrs found in parents...
                api_attrs.update(base.api_attrs)

            # Django Forms called this shadowing
            # I think it just ensures all attrs are properly represented?
            for attr, value in base.__dict__.items():
                if value is None and attr in api_attrs:
                    api_attrs.pop(attr)

        # Pass on those api attrs as an attr of the generated class
        new_class.api_attrs = api_attrs
        for key, attr in api_attrs.items():
            # Create a 'private' attr for each key. For example if
            # we want an object.term, we're going to create
            # object.__term in the init function, and here
            # we'll create object.term as a property which
            # enforces the getter and setter we want from
            # ApiAttr
            privkey = '__{}'.format(key)
            setattr(
                new_class,
                key,
                property(
                    lambda self, pk=privkey: self.__dict__.get(pk),
                    lambda self, x, pk=privkey, attr=attr: setattr(self, pk, attr.parse(x, session=self.session))
                )
            )
        return new_class


class ApiObject(metaclass=ApiObjectBase):
    def __init__(self, session, **kwargs):
        self.session = session
        self.api_attrs = copy.deepcopy(self.api_attrs)
        for key in self.api_attrs.keys():
            # Populate everything from the api_attrs if we passed kwargs
            setattr(self, '__{}'.format(key), None)
            setattr(self, key, kwargs.get(key, None))

    def fill(self):
        raise Exception('fill not implemented for {}'.format(self.__class__.__name__))
