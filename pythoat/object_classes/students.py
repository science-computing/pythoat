from .baseclass import ApiObject, ApiAttr, ApiListAttr
from .helpers import reSet
from .people import Person
from .academic import Plan
from .courses import Course


class Student (Person):
    student_id = ApiAttr(typing=int)

    def __init__(self, *args, **kwargs):
        reSet(kwargs, 'student_id', 'uw_id')
        # Moves names up one level (removes preferred_name listing)
        if 'preferred_name' in kwargs:
            kwargs.update(kwargs.pop('preferred_name'))
        super().__init__(*args, **kwargs)
        self.term_summaries = {}

    def getTermSummary(self, term):
        if term in self.term_summaries:
            return self.term_summaries[term]
        else:
            summary = self.session.getTermSummary(self, term)
            self.term_summaries[term] = summary
            return summary


class TermSummary(ApiObject):
    term = ApiAttr(typing=int, required=True)
    projected_level = ApiAttr(typing=str)
    plans = ApiListAttr(Plan)
    courses = ApiListAttr(Course)

    def __init__(self, *args, **kwargs):
        reSet(kwargs, 'term', 'term_id')
        super().__init__(*args, **kwargs)
        self.student = None
        for course in self.courses:
            course.term = self.term
