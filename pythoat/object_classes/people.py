from .baseclass import ApiObject, ApiAttr
from .helpers import reSet


class Person(ApiObject):
    user_id = ApiAttr(typing=str)
    first_name = ApiAttr(typing=str)
    last_name = ApiAttr(typing=str)

    def __init__(self, *args, **kwargs):
        reSet(kwargs, 'user_id', 'userid')
        reSet(kwargs, 'first_name', 'givennames')
        reSet(kwargs, 'last_name', 'surname')
        super().__init__(*args, **kwargs)

    @property
    def email(self):
        return '{}@uwaterloo.ca'.format(
            self.user_id
        )

    def __str__(self):
        return '{}, {} ({})'.format(
            self.first_name,
            self.last_name,
            self.user_id
        )


class Instructor(Person):
    def __init__(self, *args, **kwargs):
        reSet(kwargs, 'first_name', 'givennames')
        reSet(kwargs, 'last_name', 'surname')
        super().__init__(*args, **kwargs)

        self.first_name = kwargs.get('givennames', None)
        self.last_name = kwargs.get('surname', None)

    def __str__(self):
        return '[INSTRUCTOR] {}'.format(super().__str__())


