# Reassigns a variable from souce key unless it's already been set
def reSet(kwargs, final, source):
    kwargs[final] = kwargs.pop(source, kwargs.get(final, None))
