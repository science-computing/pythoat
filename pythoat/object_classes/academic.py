from .baseclass import ApiObject, ApiAttr, ApiListAttr
from datetime import datetime


class Plan(ApiObject):
    plan_code = ApiAttr(typing=str)
    plan_as_of = ApiAttr(
        typing=lambda x: datetime.strptime(x, '%Y-%m-%d').date()
    )
    plan_active = ApiAttr(typing=bool)
    plan_title = ApiAttr(typing=str)
    plan_short_title = ApiAttr(typing=str)
    plan_comment = ApiAttr(typing=str)
    plan_diploma_description = ApiAttr(typing=str)
    plan_transcript_description = ApiAttr(typing=str)
    degree_code = ApiAttr(typing=str)
    program_code = ApiAttr(typing=str)
    career_code = ApiAttr(typing=str)
    unit_code = ApiAttr(typing=str)
    plan_type_code = ApiAttr(typing=str)
    first_valid_term = ApiAttr(typing=int)
    last_admit_term = ApiAttr(typing=int)

    def fill(self):
        c = self.session.getPlan(self.plan_code)
        if c is not None:
            self.__dict__ = c.__dict__
        return self
