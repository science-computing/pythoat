from urllib.parse import urlencode
from .liboat import Session as lSession
from .object_classes import (
    Course,
    Section,
    Instructor,
    Student,
    TermSummary,
    Plan,
    Person,
)


class Session(lSession):
    def getPlan(self, code):
        data = self.get_json(
            '/api/v2/pps/plan/^{}$'
            .format(code)
        )
        if 'data' in data:
            data = data['data'][0]
            return Plan(self, **data)
        return None

    def getTermSummary(self, student, term):
        term = int(term)
        is_student = False
        if isinstance(student, Student):
            is_student = True
            # Did we already get this?
            if term in student.term_summaries:
                return student.term_summaries[term]
            student_id = student.student_id
        else:
            student_id = student

        # Get some data!
        data = self.get_json(
            '/api/v2/student/{}/{}/termSummary'
            .format(student_id, term)
        )['data']

        if not is_student:
            student_data = data.get('bio')
            student = Student(self, **student_data)

        termsummary = TermSummary(self, **data)
        termsummary.student = student
        student.term_summaries[term] = termsummary
        return termsummary
    
    def searchPeople(self, query, num=5, students=False, staff=True):
        encoded = urlencode(dict(
            query=query,
            num=num,
            include_students=str(students).lower(),
            include_staff=str(staff).lower()
        ))
        print(encoded)
        data = self.get_json(
            '/api/v2/util/searchSuggestions?{}'
            .format(
                encoded
            )
        )['data']
        return [Person(self, **x) for x in data]