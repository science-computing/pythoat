from setuptools import setup

setup(
    name='pythoat',
    version='0.0.1',
    description='Object-based python library based on liboat',
    url='',
    author='Mirko Vucicevich',
    author_email='mvucicev@uwaterloo.ca',
    license='Unlicense',
    packages=['pythoat'],
    install_requires=[
        'requests>=2.20',
        'PyJWT>=1.6',
    ],
    test_suite='nose.collector',
    tests_require=['nose'],
    zip_safe=False
)
